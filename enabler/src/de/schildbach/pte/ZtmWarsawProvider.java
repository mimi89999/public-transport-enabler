/*
 * Copyright 2010-2018 the original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.schildbach.pte;

import de.schildbach.pte.dto.Product;

import okhttp3.HttpUrl;

/**
 * @author Andreas Schildbach
 * @author Michel Le Bihan
 */
public class ZtmWarsawProvider extends AbstractHafasLegacyProvider {
    private static final HttpUrl API_BASE = HttpUrl.parse("http://wyszukiwarka.ztm.waw.pl/bin/");
    private static final Product[] PRODUCTS_MAP = { null, null, Product.HIGH_SPEED_TRAIN, Product.REGIONAL_TRAIN, Product.SUBURBAN_TRAIN, Product.BUS,
            Product.TRAM, Product.SUBWAY, null, Product.FERRY };

    public ZtmWarsawProvider() {
        super(NetworkId.ZTMWARSAW, API_BASE, "pn", PRODUCTS_MAP);
    }

    @Override
    public boolean hasCapability(final Capability capability) {
        if (capability == Capability.DEPARTURES) {
            return true;
        } else {
            return super.hasCapability(capability);
        }
    }

    @Override
    protected Product normalizeType(final String type) {
        final String ucType = type.toUpperCase();

        // Subway
        if ("METRO".equals(ucType)) {
            return Product.SUBWAY;
        }

        // Usually the Product is the line number.

        // Suburban Trains
        if (ucType.startsWith("S")) {
            return Product.SUBURBAN_TRAIN; // Szybka Kolej Miejska
        }

        // Suburban Trains
        if (ucType.startsWith("R")) {
            return Product.REGIONAL_TRAIN; // Koleje Mazowieckie maybe?
        }

        try {
            final int lineNumber = Integer.parseInt(type);

            // Tram
            // All line numbers up to 99 are trams
            // Bus line numbers start at 100
            if (lineNumber < 100) {
                return Product.TRAM;
            } else {
                return Product.BUS;
            }
        } catch (NumberFormatException e) {
            // If we don't know what it is, it is most likely a bus
            return Product.BUS;
        }
    }
}
